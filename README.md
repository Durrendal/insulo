# What?

Insulo - Esperanto - Island

An offline first API endpoint emulator, primarily designed so that I can tinker with pre-seeded per project data while traveling.

## Build & Install

With make!
```
make nimble
make build
make install
```

Without make!
```
nimble install yaml
nim -c -d:release src/insulo.nim
sudo cp src/insulo /usr/local/bin/insulo
```

## Usage:

Insulo expects that you have a valid API endpoint against which you're developing. In essence you need to have you JSON returns pre-seeded for it to work. I suppose you could also hand craft JSON to return in a sort of prototypal sense as well, it's essentially the same.

When running insulo for the first time you'll need to create a new config file with
```
insulo init
```

Modify the config file with the endpoints you want to respond on, the path's to the json you want to return, what type of return and the http code to respond with.
```
Host: 127.0.0.1
Port: 8080
Endpoints:
  - Endpoint: "/api/v1/teams?sportId=12"
    Json: "teams.json"
    Type: "application/json"
    Code: 200
  - Endpoint: "/api/v1/schedule?sportId=12&amp;TeamCode=prt"
    Json: "schedule.json"
    Type: "application/json"
    Code: 200
  - Endpoint: "/api/v1/game/724328/linescore"
    Json: "linescore.json"
    Type: "application/json"
    Code: 200
```

After setting this up, seed your json files with valid return data. You'll end up with something like this below. These examples are all from a MLB linescore scraper I'm working on. I personally like to make a .insulo directory inside of the src repo of my project.

```
.insulo|>> tree
.
├── insulo.conf
├── linescore.json
├── schedule.json
└── teams.json

1 directory, 6 files

```

Once it's setup, run insulo from the directory containing your json data and configuration file and you're ready to go!
```
cd ~/Development/MILBScrape/src/.insulo
insulo serve
```

Once running all you need to do is query the configured endpoints! For the moment it's limited to exactly what you configure, and only GET requests, but I want to add POST/PUT functionality and regex completion on the endpoints so they can be more flexible. 