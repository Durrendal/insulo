nimble:
	nimble install yaml

build:
	cd src && nim c -d:release insulo.nim

install:
	mv src/insulo /usr/local/bin/insulo
