import std/[os, asynchttpserver, asyncdispatch, streams, uri], yaml

const doc = """
insulo [-h] [-i] [-s]

Usage:
  insulo init     generate an insulo.conf file
  insulo serve    run the insulo endpoint server
  insulo help     helpful help messages
"""

const insuloTmpl = """
Host: 127.0.0.1
Port: 8080
Debug: true
Endpoints:
  - Endpoint: "/api/endpoint"
    Json: "/path/to/json/file"
    Type: "application/json"
    Code: 200
"""

type Endpoint = tuple
  Endpoint: string
  Json: string
  Type: string
  Code: int

type Conf = object
    Host: string
    Port: int
    Debug: bool
    Endpoints: seq[Endpoint]

var config: Conf

#Load yaml file to config struct
proc configure() =
  if fileExists("insulo.conf"):
    var f = newFileStream("insulo.conf")
    load(f, config)
    f.close()
    for ep in config.Endpoints:
      echo "Listening @ " & ep.Endpoint
  else:
    echo "insulo.conf cannot be found in the current directory!"
    quit(1)

proc generateTemplate() =
  if fileExists("insulo.conf"):
    echo "insulo.conf exists at current path"
    quit(1)
  else:
    var
      f = open("insulo.conf", fmWrite)
    f.writeLine(insuloTmpl)
    echo "Generated insulo.conf"

#Return the full contents of a file as a string
proc getContents(file: string): string  =
  if fileExists(file):
    let
      contents = readFile(file)
    return contents
  else:
    return file & " not found on host!\n"

proc intToHttp(code: int): HttpCode =
  case code:
    of 200:
      return Http200
    of 301:
      return Http301
    of 302:
      return Http302
    of 403:
      return Http403
    of 404:
      return Http404
    of 410:
      return Http410
    of 500:
      return Http500
    of 503:
      return Http503
    else:
      return Http418

type HResp = tuple[
  code: HttpCode,
  content: string,
  headers: HttpHeaders]

#Generate an http response
#This checks the request path of the url ie: /api/v1/teams and returns the json file defined in the config file
#TODO: flexible content type, POST/PUT etc functionality defined by the yaml file
proc responder(req: Request): HResp {.gcsafe.} =
  if config.Debug == true:
      echo req.url
      echo req.headers
      echo req.reqMethod
      echo req.body
  for ep in config.Endpoints:
    if $req.url == ep.Endpoint:
      echo ep.Endpoint & " requested"
      let
        res = (code: intToHttp(ep.Code), content: getContents(ep.Json), headers: newHttpHeaders({"Content-Type": ep.Type}))
      return res
  #if nothing matches return invalid
  echo "Invalid request: " & $req.url
  let
    res = (code: Http500, content: "Invalid request: " & $req.url & "\n", headers: newHttpHeaders({"Content-Type": "plain/text; charset=utf-8"}))
  return res

#Listen on the port/host defined in the config and async dispatch http requests
proc serve() {.async.} =
  var
    server = newAsyncHttpServer()
  proc handle(req: Request): Future[void] {.async.} =
    var res: HResp
    res = responder(req)
    await req.respond(res.code, res.content, res.headers)
  server.listen(Port(config.Port), config.Host)
  while true:
    if server.shouldAcceptRequest():
      await server.acceptRequest(handle)
    else:
      await sleepAsync(500)

proc main(): void =
  var
    args = commandLineParams()
  if len(args) == 1:
      case args[0]:
        of "-h", "--help", "help":
          echo doc
        of "-i", "--init", "init":
          generateTemplate()
        of "-s", "--serve", "serve":
          configure()
          waitFor(serve())
        else:
          echo doc
  else:
          echo doc

when isMainModule:
  main()
